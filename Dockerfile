FROM java:8
RUN useradd -m test-java --uid=1000 
USER test-java
WORKDIR /home/test-java
ADD ./target/*.jar /app.jar
ADD ./run.sh /run.sh
RUN chmod a+x /run.sh
CMD /run.sh
