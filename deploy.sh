#!/usr/bin/env bash

TAG=${1}
export BUILD_NUMBER=${TAG}
for f in ./deploy/*.yaml
do
  envsubst < $f > "./deploy/$(basename $f)"
done

kubectl apply -f ./deploy/
